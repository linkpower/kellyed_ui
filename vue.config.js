const path = require('path')

module.exports = {
  publicPath: process.env.BASE_URL,
  filenameHashing: false,
  configureWebpack: {
    devtool: 'source-map',
    performance: {
      maxAssetSize: 1000000
    } 
  },
  chainWebpack: config => {
    // api-client
    const apiClient = process.env.VUE_APP_API_CLIENT || 'server' // mock or server
    config.resolve.alias.set(
      'api-client', path.resolve(__dirname, `src/api/${apiClient}`)
    )
  },
  pluginOptions: {
    sassResources: {
      resources: './src/**/*.scss'
    },
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false
    }
  },
  "transpileDependencies": [
    "vuetify"
  ]
}