# KellyEd Frontend UI

## Dev Login Account
Admin account: admin@a.com / admin

## Pre-request
If you haven't install pnpm, do the following steps:
```
npm install pnpm -g
```

## Upgrade vue-cli
Upgrade vue-cli to @vue/cli if you haven't done before
```
pnpm uninstall vue-cli -g
pnpm install @vue/cli @vue/cli-service-global -g
```

## Project setup
```
pnpm install
```

### Compiles and hot-reloads for development
```
pnpm run serve
```

### Compiles and minifies for production
```
pnpm run build
```

### Lints and fixes files
```
pnpm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Package Reference
* [vue](https://vuejs.org) Vue Framework
* [vuetify](https://vuetifyjs.com/en/) Vuetify is a Vue UI Library with beautifully handcrafted Material Components
* [@mdi/font](https://dev.materialdesignicons.com/icons) Material Design Icons