import Vue from 'vue';
import VueRouter from 'vue-router';
// import HelloWorld from  '@/components/HelloWorld';
import Dashboard from '@/components/Dashboard';
import Login from '@/components/Login';
import StaffList from '@/components/Users/StaffList';
import Staff from '@/components/Users/Staff';
import StudentList from '@/components/Users/StudentList';
import Student from '@/components/Users/Student';
import TeacherList from '@/components/Users/TeacherList';
import Teacher from '@/components/Users/Teacher';
import ProductList from '@/components/School/ProductList';
import Product from '@/components/School/Product';
import CourseList from '@/components/School/CourseList';
import Course from '@/components/School/Course';
import LessonList from '@/components/School/LessonList';
import Lesson from '@/components/School/Lesson';
import SalesOrderList from '@/components/Sales/SalesOrderList';
import SalesOrder from '@/components/Sales/SalesOrder';
import PaymentList from '@/components/Sales/PaymentList';
import Payment from '@/components/Sales/Payment';
import ChoiceList from '@/components/Admin/ChoiceList';
import Export from '@/components/Admin/Export';
import InvalidPage from '@/components/InvalidPage';
import store from '../store';

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        redirect: '/Dashboard'
    },
    {
        name: 'Login',
        path: '/Login',
        component: Login
    },
    {
        name: 'Logout',
        path: '/Logout',
        component: Login
    },
    {
        name: 'Dashboard',
        path: '/Dashboard',
        component: Dashboard
    },
    // Users
    {
        name: 'StaffList',
        path: '/Users/Staffs',
        component: StaffList
    },
    {
        name: 'Staff',
        path: '/Users/Staffs/:id',
        component: Staff
    },
    {
        name: 'StudentList',
        path: '/Users/Students',
        component: StudentList
    },
    {
        name: 'Student',
        path: '/Users/Students/:id',
        component: Student
    },
    {
        name: 'TeacherList',
        path: '/Users/Teachers',
        component: TeacherList
    },
    {
        name: 'Teacher',
        path: '/Users/Teachers/:id',
        component: Teacher
    },

    // School
    {
        name: 'CourseList',
        path: '/School/Courses',
        component: CourseList
    },
    {
        name: 'Course',
        path: '/School/Courses/:id',
        component: Course
    },
    {
        name: 'LessonList',
        path: '/School/Lessons',
        component: LessonList
    },
    {
        name: 'Lesson',
        path: '/School/Lessons/:id',
        component: Lesson
    },
    {
        name: 'ProductList',
        path: '/School/Products',
        component: ProductList
    },
    {
        name: 'Product',
        path: '/School/Products/:id',
        component: Product
    },

    // Sales
    {
        name: 'SalesOrderList',
        path: '/Sales/SalesOrders',
        component: SalesOrderList
    },
    {
        name: 'SalesOrder',
        path: '/Sales/SalesOrders/:id',
        component: SalesOrder
    },
    {
        name: 'PaymentList',
        path: '/Sales/Payments',
        component: PaymentList
    },
    {
        name: 'Payment',
        path: '/Sales/Payments/:id',
        component: Payment
    },

    
    // Admin
    {
        name: 'ChoiceList',
        path: '/Admin/Choices',
        component: ChoiceList
    },
    {
        name: 'Import',
        path: '/Admin/Import',
        component: Export
    },
    {
        name: 'Export',
        path: '/Admin/Export',
        component: Export
    },
    {
        path: '*',
        component: InvalidPage
    }


    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    // }
]

const router = new VueRouter({
//   mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
    console.debug(`router before: ${from.path} => ${to.path}`);
    // check user login for authorized pages
    if (router.prevRoute && to.name != "Login" && to.name != "Logout") {
        const curUser = store.getters.curUser;
        // console.debug("curUser:", curUser);
        if (!curUser) {
            console.debug(`Denied to ${to.path}. Redirect to Login page`);
            router.prevRoute = to;
            router.postLoginPath = to;
            router.prevRoute = to;
            next({ path: "/Login" });
            return;
        }
    }
    // save previous route in router
    router.prevRoute = from;
    next();
});

// prevent NavigationDuplicated exception
['push','replace'].forEach(method => {
const originalMethod = VueRouter.prototype[method];
    VueRouter.prototype[method] = function m(location) {
        return originalMethod.call(this, location).catch(error => {
            if (error.name !== 'NavigationDuplicated') {
                // capture exception
            }
        })
    };
});

export default router