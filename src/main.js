import Vue from 'vue';
import App from './App.vue';
import VueMq from 'vue-mq';
import vuetify from './plugins/vuetify';
import i18n from './i18n';
import router from './router';
import store from './store';
import './theme/common.scss';

Vue.config.productionTip = false

Vue.use(VueMq, {
  breakpoints: { // default breakpoints - customize this
    sm: 600,
    md: 1264,
    lg: Infinity,
  },
  defaultBreakpoint: 'sm' // customize this for SSR
})

new Vue({
  router,
  store,
  i18n,
  vuetify,
  render: h => h(App)
}).$mount('#app')