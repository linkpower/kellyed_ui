import axios from 'axios';
import store from '../../store';

function getServerUrl(command) {
    return (process.env.VUE_APP_SERVER_URL || '') + command
}
export default {
    getHeader(name) {
        return axios.defaults.headers.common[name]
    },
    setHeaders(headers) {
        axios.defaults.headers.common = {...axios.defaults.headers.common, ...headers}
    },
    getServerUrl: getServerUrl,
    paramsToStr(params) {
        let s = ""
        for (let [key, value] of Object.entries(params)) {
            if (s != "") s += "&"
            s += key + "=" + value
        }
        return s
    },
    updateToken(res) {
        const token = res.data;
        if (token && token.token_type && token.access_token) {
            axios.defaults.headers.common['Authorization'] = 
                token.token_type + ' ' + token.access_token;
        }
        if (token && token.refresh_token) {
            // save refresh token when access token expired, brower refresh or new window/tab
            localStorage.setItem('refreshToken', token.refresh_token);
        }
        return res.data;
    },
    axiosGet(url, params, config) {
        if (!config)
            config = {}
        if (params)
            config['params'] = params
        let params_str = params && (", params: " + this.paramsToStr(params)) || ''
        console.debug(`fetch: ${url}${params_str}`)
        return axios.get(getServerUrl(url), config)
    },
    axiosPost(url, data, config) {
        console.debug(`post data: ${url}`)
        return axios.post(getServerUrl(url), data, config)
    },
    axiosPut(url, data, config) {
        console.debug(`put data: ${url}`)
        return axios.put(getServerUrl(url), data, config)
    },
    axiosDelete(url, config) {
        console.debug(`delete data: ${url}`)
        return axios.delete(getServerUrl(url), config)
    },
    resResult(res) {
        return res.data;
    },

    // generic rest API
    fetchCount(name, criteria) {
        return this.axiosGet(`/api/${name}/count`, criteria).then(res => res.data)
    },
    fetchList(name, criteria) {
        return this.axiosGet(`/api/${name}`, criteria).then(res => res.data)
    },
    fetch(name, user_id) {
        return this.axiosGet(`/api/${name}/${user_id}`).then(res => res.data)
    },
    post(name, data) {
        return this.axiosPost(`/api/${name}`, data).then(res => res.data)
    },
    put(name, id, data) {
        return this.axiosPut(`/api/${name}/${id}`, data).then(res => res.data)
    },
    delete(name, id) {
        return this.axiosDelete(`/api/${name}/${id}`).then(res => res.data)
    },


    // admin
    postImport(data){
        return this.axiosPost("/api/admin/import", data).then(res => res.data)
    },
    postExport(data){
        return this.axiosPost("/api/admin/export", data, {
            headers: { 'Content-Type': 'multipart/form-data' },
            responseType: 'blob'
        }).then(res => res.data)
    },

    // login
    postLogin(data){
        return this.axiosPost("/api/auth/login", data).then(this.updateToken);
    },
    fetchRefresh(token){
        if (!token)
            token = localStorage.getItem('refreshToken');
        const auth = token ? 'Bearer ' + token : '';
        return this.axiosGet("/api/auth/refresh", {
            headers: { 'Authorization': auth },
        }).then(this.updateToken);
    },
    fetchCurrentUser(){
        return this.axiosGet('/api/auth/current_user').then(res => res.data)
    }
}


// Add a response interceptor
axios.interceptors.response.use((response) => response, 
    function (error) {
        const originalRequest = error.config;
        const refreshToken = localStorage.getItem('refreshToken');
        // status 401 with refreshToken, try to refresh access token
        if (error.response && error.response.status === 401 && !originalRequest._retry && refreshToken) {
            originalRequest._retry = true;
            console.debug('refresh access token:');
            return axios.get(getServerUrl("/api/auth/refresh"), {
                _retry: true,
                headers: { 'Authorization': `Bearer ${refreshToken}` },
            })
            .then(res => {
                if (res.status === 200) {
                    // Change Authorization header
                    const token = res.data;
                    if (token && token.access_token) {
                        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token.access_token;
                        console.debug('got new access token:');
                    } else {
                        console.warn('no access token:');
                    }
                    // update current user
                    return axios.get(getServerUrl("/api/auth/current_user")).then(res => {
                        console.debug('got current user:', res.data, store);
                        // update current user
                        store.commit('setCurUser', res.data);
                        // return originalRequest object with Axios.
                        return axios(originalRequest);
                    })
                    .catch(error => Promise.reject(error));
                } else {
                    // remove access token and refresh token
                    const msg = res.data && res.data.detail;
                    if (msg && msg.endsWith('token expired')) {
                        console.error('refresh token expired');
                        localStorage.removeItem('refreshToken');
                    }
                    return Promise.reject(error);
                }
            })
            .catch(error => {
                console.error(error);
                // remove refresh token if any other problem
                localStorage.removeItem('refreshToken');
                return Promise.reject(error);
            });
        }

        return Promise.reject(error);
    }
)