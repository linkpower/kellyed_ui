import choices from './data/choices.json';
import staffs from './data/staffs.json';
import students from './data/students.json';
import teachers from './data/teachers.json';
import products from './data/products.json';
import courses from './data/courses.json';
import lessons from './data/lessons.json';

const mock = { choices, staffs, students, teachers, products, courses, lessons };
const mock_ids = { 
    choices: 'choice_id', 
    staffs: 'user_id', 
    students: 'user_id', 
    teachers: 'user_id', 
    products: 'product_id', 
    courses: 'course_id', 
    lessons: 'lesson_id',
};
const delayTime = 0;
const promisify = (mockData, error) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (error)
                reject(error);
            else
                resolve(mockData);
        }, delayTime);
    })
}

export default {
    setHeaders(headers) {
        console.info("axios set headers:", JSON.stringify(headers));
    },

    // generic rest API
    fetchCount(name, criteria) {
        const items = mock[name];
        return promisify({count: items.length});
    },
    fetchList(name, criteria) {
        if (!criteria)
            criteria = {page: 0, page_size: 0};
        const [name0, name1] = name.split('/')
        let items = mock[name0];
        const {page, page_size} = criteria;
        const start = page * page_size;
        const end = (page + 1) * page_size;
        if (name0 == 'choices' && name1)
            items = items.filter(x => x.category == name1);
        // console.debug('fetchList:', name, items);
        return promisify(page_size ? items.slice(start, end) : items, null);
    },
    fetch(name, id) {
        const items = mock[name];
        const item = items.find((x) => x[mock_ids[name]] == id);
        const error = item ? null : `${name} "${id}" not found`;
        // console.debug('fetch:', item);
        return promisify({...item}, error);
    },
    post(name, data) {
        // create
        let error = null;
        const items = mock[name];
        const last = items[items.length - 1];
        data[mock_ids[name]] = last[mock_ids[name]] + 1;
        items.push(data) 
        console.debug(`added: ${name}`, data[mock_ids[name]]);
        return promisify({...data}, error)
    },
    put(name, id, data) {
        // update
        let error = null;
        const items = mock[name];
        const index = items.findIndex((x) => x[mock_ids[name]] == id);
        if (index >= 0)
            items[index] = data;
        else
            error = `${name} "${id}" not found`;
        return promisify({...data}, error)
    },
    delete(name, id) {
        const items = mock[name];
        const index = items.findIndex((x) => x[mock_ids[name]] == id);
        let error = null
        console.debug("delete:", id, index);
        if (index >= 0)
            items.splice(index, 1);
        else
            error = `${name} "${id}" not found`;
        return promisify({id: id}, error)
    },

    // admin
    postImport(data) {
        return promisify({
            imported: 10, 
            messages: ["User, imported 3", "Student, imported 4"]
        });
    },
    postExport(data) {
        return promisify("");
    },

    // login
    postLogin(data) {
        return promisify({
            token_type: "bear", 
            access_token: "1231231234", 
            expire_time: 1613593077
        });
    },
    fetchLogout() {
        return promisify("OK");
    },
    fetchCurrentUser() {
        return promisify({
            name: "Admin", 
            roles: ["Admin"]
        })
    }
}