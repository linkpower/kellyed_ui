import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader
import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
// Translation provided by Vuetify (javascript)
import en from 'vuetify/es5/locale/en';
import zhHans from 'vuetify/es5/locale/zh-Hans';
import zhHant from 'vuetify/es5/locale/zh-Hant';

Vue.use(Vuetify);

export default new Vuetify({
	lang: {
		locales: { en, zhHant, zhHans },
		current: 'en',
	},
	icons: {
		iconfont: 'mdi', // default - only for display purposes
	},
	customVariables: ['./theme/vuetify_vars.scss'],
	treeShake: true,
});
