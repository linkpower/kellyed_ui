import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

function loadValue(name) {
  const item = localStorage.getItem(name);
  let value = item;
  if (item) {
    try {
      value = JSON.parse(item);
    } catch {
      //
    }
  }
  // console.log("loadValue:", name, item)
  return value;
}

function saveValue(name, value) {
  if (value == null)
    localStorage.removeItem(name);
  else
    localStorage.setItem(name, JSON.stringify(value));
}

export default new Vuex.Store({
  state: {
    curUser: null,
    snackMessage: {},
    errorMessage: {},
    itemsPerPage: 10,
  },

  getters: {
    curUser: state => state.curUser,
    snackMessage: state => state.snackMessage,
    errorMessage: state => state.errorMessage,
    itemsPerPage: state => state.itemsPerPage,
  },

  mutations: {
    setCurUser(state, user) {
      console.debug("setCurUser:", JSON.stringify(user));
      if (state.curUser !== user) {
        saveValue('curUser', user);   // save to storage
        state.curUser = user;
      }
    },
    setSnackMessage(state, message) {
      state.snackMessage = {
        snackbar: true,
        text: message.text,
        color: message.color || "blue",
        timeout: message.timeout || 2000
      };
    },
    setErrorMessage(state, error) {
      if (!error) {
        state.errorMessage = {
          status: 200,
          title: '',
          message: '',
          open: false
        };
        return
      }
      let status = 200;
      let {title, message, info, details} = error;
      if (message.response)
        status = message.response.status;
      if (!message && title) {
        message = title
        title = 'Error'
      } else if (message.response && message.response.data && 
          (message.response.data.detail || message.response.data.message)) {
        message = message.response.data.detail || message.response.data.message;
      } else if (message.message || message.detail) {
        message = message.message || message.detail
      }
      if (info)
        console.info(title ? `${title}: ${message}` : message);
      else
        console.error(title ? `${title}: ${message}` : message);
      state.errorMessage = {
        info: info,
        status,
        title,
        message,
        details,
        open: true
      };
    },
    setItemsPerPage(state, value) {
      state.itemsPerPage = value;
    }
  },

  actions: {
    // restore session values
    restoreSession(context) {
      // commit to restore curUser
      context.commit('setCurUser', loadValue('curUser'));
      console.log("restore session:", JSON.stringify(context.state));
    }
  }
})